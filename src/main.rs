use std::process::Command;
use std::error::Error;
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

// https://docs.rs/clap/2.33.0/clap/
// https://raw.githubusercontent.com/clap-rs/clap/master/examples/01a_quick_example.rs
extern crate clap;

use clap::{App, Arg, SubCommand};

struct Patch {
    id: String,
    state: String,
    name: String,
//    msgid: String,
}

impl fmt::Display for Patch {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{} {} {}", self.id, self.state, self.name)
    }
}

impl Patch {
    fn download_patch(&self, data_dir: &String) -> Result<String, String> {
        let directory = match self.prepare_directory(&data_dir) {
            Ok(directory) => directory,
            Err(_) => return Err("Cannot create directory for download".to_string())
        };

        let current_dir = match std::env::current_dir() {
            Ok(dir) => dir,
            Err(_) => return Err("Cannot get current directory".to_string())
        };

        if std::env::set_current_dir(Path::new(&directory)).is_err() {
            return Err("Cannot set current directory".to_string());
        }

        let output = Command::new("pwclient")
            .arg("get")
            .arg(&self.id)
            .output()
            .expect("Failed to spawn pwclient child process");

        if std::env::set_current_dir(Path::new(&current_dir)).is_err() {
            return Err("Cannot set directory back".to_string());
        }

        Ok(String::from_utf8(output.stdout).unwrap_or("download_patch() failed".to_string()))
    }

    fn get_directory(&self, data_dir: &String) -> String {
        let mut dir = data_dir.clone();
        dir.push_str("/");
        dir.push_str(&self.id);
        dir
    }

    fn prepare_directory(&self, data_dir: &String) -> Result<String, String> {
        let dir = self.get_directory(data_dir);
        fs::create_dir_all(&dir).unwrap_or_else(|why| {
            println!("! {:?}", why.kind());
        });
        Ok(dir)
    }

    fn read_patch(&self, data_dir: &String) -> Result<String,String> {
        let directory = self.get_directory(data_dir);

        let current_dir = match std::env::current_dir() {
            Ok(dir) => dir,
            Err(_) => return Err("Cannot get current directory".to_string())
        };

        if std::env::set_current_dir(Path::new(&directory)).is_err() {
            return Err("Cannot set current directory".to_string());
        }

        match fs::read_dir("./") {
            Err(why) => panic!("! {:?}", why.kind()),
            Ok(mut paths) => loop {
                let path = match paths.next() {
                    Some(path) => path,
                    _ => panic!("didnt find anything")
                };
                // println!("> {:?}", path.unwrap().path());
                let mut file = match File::open(&path.unwrap().path()) {
                    // The `description` method of `io::Error` returns a string that
                    // describes the error
                    Err(why) => panic!("couldn't open {}", 
                                       why.description()),
                    Ok(file) => file,
                };

                // Read the file contents into a string, returns `io::Result<usize>`
                let mut s = String::new();
                file.read_to_string(&mut s).unwrap();
                if std::env::set_current_dir(Path::new(&current_dir)).is_err() {
                    return Err("Cannot set directory back".to_string());
                };
                break Ok(s)
            },
        }
    }
}



fn get_patches(data_dir: &String, cached: bool) -> Vec<Patch> {
    let patchlist_file = format!("{data_dir}/patch.list", data_dir = data_dir);
    let output_data = if cached {
        let path = Path::new(&patchlist_file);
        let mut file = match File::open(&path) {
            // The `description` method of `io::Error` returns a string that
            // describes the error
            Err(why) => panic!("couldn't open {}: {}", path.display(),
                               why.description()),
            Ok(file) => file,
        };

        // Read the file contents into a string, returns `io::Result<usize>`
        let mut s = String::new();
        match file.read_to_string(&mut s) {
            Err(why) => panic!("couldn't read {}: {}", path.display(),
                               why.description()),
            Ok(_) => s,
        }
    } else {
        let output = Command::new("pwclient")
            .arg("list")
            .arg("-s")
            .arg("New")
            .arg("-f")
            .arg("%{id};%{state};%{name}")
            .output()
            .expect("Failed to spawn pwclient child process");
        let output_data = String::from_utf8(output.stdout).unwrap();
        match save_data(patchlist_file, &output_data) {
            Ok(output) => println!("{}", output),
            Err(output) => print!("{}", output)
        };
        output_data
    };

    let mut result = Vec::new();
    let lines = output_data.as_str().lines();
    for line in lines {
        // let line = lines.next().unwrap();
        let mut index = line.find(';').unwrap();
        let (id, line_state_name) = line.split_at(index);
        index = line_state_name[1..].find(';').unwrap();
        let (state, line_name) = line_state_name[1..].split_at(index);
        let (_, name) = line_name.split_at(1);

        // dbg!(id, state, name);
        result.push(Patch {
            id: String::from(id),
            state: String::from(state),
            name: String::from(name)
        })
    }
    result
}

fn save_data(file: String, data: &String) -> Result<String, String> {
    let path = Path::new(&file);
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.description()),
        Ok(file) => file,
    };

    match file.write_all(data.as_bytes()) {
        Err(why) => {
            println!("couldn't write to {}: {}", display, why.description());
            Err(why.description().to_string())
        }
        Ok(_) => {
            println!("successfully wrote to {}", display);
            Ok("successfully wrote".to_string())
        }
    }
}

fn download_all_patches(data_dir: &String, patches: &Vec<Patch>) -> bool {
    for patch in patches {
        match patch.download_patch(&data_dir) {
            Ok(output) => println!("Created {} - {}", patch.get_directory(&data_dir), output),
            Err(output) => println!("{}", output)
        };
    };
    true
}

// https://doc.rust-lang.org/std/fs/fn.metadata.html

#[cfg(test)]
mod tests {
    use super::*;

    static DATA_DIR: &'static str = "tests";

    #[test]
    fn test_get_patches() {
        let patches = get_patches(&DATA_DIR.to_string(), true);
        assert_ne!(patches.len(), 0);
    }

    #[test]
    fn test_patch_read_patch() {
        let data_dir = &DATA_DIR.to_string();
        let patches = get_patches(&data_dir, true);
        assert_ne!(patches.len(), 0);
        let s = patches[0].read_patch(&data_dir);
        assert!(s.is_ok());
    }

    #[test]
    fn test_patch_prepare_directory() {
        let data_dir = &DATA_DIR.to_string();
        let test_data_dir = &"tests/data".to_string();
        let patches = get_patches(&data_dir, true);
        assert_ne!(patches.len(), 0);
        assert!(patches[0].prepare_directory(&test_data_dir).is_ok());
        fs::remove_dir_all(&test_data_dir).unwrap();
    }
}

fn main() {
    // let data_dir = String::from("data");

    let matches = App::new("selinux-patchwork")
        .version("0.1.1")
        .author("Petr Lautrbach <brs@o5za5.cz>")
        .about("Download and filer patches from patchwork.kernel.org")
        .arg(Arg::with_name("datadir")
             .short("d")
             .long("datadir")
             .value_name("datadir")
             .help("Sets a custom data directory")
             .takes_value(true))
        .arg(Arg::with_name("cached")
             .short("f")
             .long("fetch")
             .value_name("cached")
             .help("Fetch patches from patchwork")
             .takes_value(false))
        .get_matches();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    let data_dir = matches.value_of("datadir").unwrap_or("data");
    let cached = ! matches.is_present("cached");


    // You can check the value provided by positional arguments, or option arguments
    if let Some(o) = matches.value_of("datadir") {
        println!("Value for output: {} {:?}", o, cached);
    }

    let patches = get_patches(&data_dir.to_string(), cached);

    // download_all_patches(&data_dir, &patches);

//    let patch = patches[0].read_patch(&data_dir.to_string()).unwrap();
    // let directory = patches[0].get_directory(&data_dir);
//    println!("{}", patch);
}
