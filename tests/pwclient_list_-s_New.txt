8943551;New;[v4,02/21] fs: Remove check of s_user_ns for existing mounts in fs_fully_visible()
8943601;New;[v4,04/21] block_dev: Support checking inode permissions in lookup_bdev()
8943591;New;[v4,08/21] userns: Replace in_userns with current_in_userns
8943421;New;[v4,10/21] fs: Check for invalid i_uid in may_follow_link()
8943541;New;[v4,11/21] cred: Reject inodes with invalid ids in set_create_file_as()
8943611;New;[v4,13/21] fs: Update posix_acl support to handle user namespace mounts
8943631;New;[v4,14/21] fs: Allow superblock owner to change ownership of inodes with unmappable ids
8943581;New;[v4,16/21] fs: Allow superblock owner to access do_remount_sb()
8943641;New;[v4,18/21] fuse: Add support for pid namespaces
8943651;New;[v4,19/21] fuse: Support fuse filesystems outside of init_user_ns
8944461;New;[v4,01/21] fs: fix a posible leak of allocated superblock
8944471;New;[v4,02/21] fs: Remove check of s_user_ns for existing mounts in fs_fully_visible()
8944481;New;[v4,03/21] fs: Allow sysfs and cgroupfs to share super blocks between user namespaces
8944491;New;[v4,04/21] block_dev: Support checking inode permissions in lookup_bdev()
8944501;New;[v4,05/21] block_dev: Check permissions towards block device inode when mounting
8944531;New;[v4,06/21] fs: Treat foreign mounts as nosuid
8944511;New;[v4,07/21] selinux: Add support for unprivileged mounts from user namespaces
8944541;New;[v4,08/21] userns: Replace in_userns with current_in_userns
8944561;New;[v4,09/21] Smack: Handle labels consistently in untrusted mounts
8944571;New;[v4,10/21] fs: Check for invalid i_uid in may_follow_link()
8944581;New;[v4,11/21] cred: Reject inodes with invalid ids in set_create_file_as()
8944591;New;[v4,12/21] fs: Refuse uid/gid changes which don't map into s_user_ns
8944601;New;[v4,13/21] fs: Update posix_acl support to handle user namespace mounts
8944611;New;[v4,14/21] fs: Allow superblock owner to change ownership of inodes with unmappable ids
8944621;New;[v4,15/21] fs: Don't remove suid for CAP_FSETID in s_user_ns
8944631;New;[v4,16/21] fs: Allow superblock owner to access do_remount_sb()
8944641;New;[v4,17/21] capabilities: Allow privileged user in s_user_ns to set security.* xattrs
8944651;New;[v4,18/21] fuse: Add support for pid namespaces
8944661;New;[v4,19/21] fuse: Support fuse filesystems outside of init_user_ns
8944671;New;[v4,20/21] fuse: Restrict allow_other to the superblock's namespace or a descendant
8944681;New;[v4,21/21] fuse: Allow user namespace mounts
10905761;New;libselinux: Ignore the stem when looking up all matches in file context
10971701;New;[V2] selinux: Fix strncpy in libselinux and libsepol
10972685;New;[V2,1/2] libselinux: Save digest of all partial matches for directory
10975829;New;[V2,2/2] setfiles: Update utilities for the new digest scheme
10972719;New;[RFC,V3] selinux-testsuite: Add test for restorecon
10984123;New;[RFC,v1,1/3] LSM/x86/sgx: Add SGX specific LSM hooks
10984137;New;[RFC,v1,2/3] LSM/x86/sgx: Implement SGX specific hooks in SELinux
10984129;New;[RFC,v1,3/3] LSM/x86/sgx: Call new LSM hooks from SGX subsystem
10993621;New;libsepol: error in CIL if a permission cannot be resolved
11005459;New;[RFC,v4,01/12] x86/sgx: Use mmu_notifier.release() instead of per-vma refcounting
11005445;New;[RFC,v4,02/12] x86/sgx: Do not naturally align MAP_FIXED address
11005421;New;[RFC,v4,03/12] selftests: x86/sgx: Mark the enclave loader as not needing an exec stack
11005435;New;[RFC,v4,04/12] x86/sgx: Require userspace to define enclave pages' protection bits
11005437;New;[RFC,v4,05/12] x86/sgx: Enforce noexec filesystem restriction for enclaves
11005475;New;[RFC,v4,06/12] mm: Introduce vm_ops->may_mprotect()
11005449;New;[RFC,v4,07/12] LSM: x86/sgx: Introduce ->enclave_map() hook for Intel SGX
11005457;New;[RFC,v4,08/12] security/selinux: Require SGX_MAPWX to map enclave page WX
11005489;New;[RFC,v4,09/12] LSM: x86/sgx: Introduce ->enclave_load() hook for Intel SGX
11005465;New;[RFC,v4,10/12] security/selinux: Add enclave_load() implementation
11005463;New;[RFC,v4,11/12] security/apparmor: Add enclave_load() implementation
11005481;New;[RFC,v4,12/12] LSM: x86/sgx: Show line of sight to LSM support SGX2's EAUG
11010339;New;[V2,2/2] selinux: Update manpages after removing legacy boolean and user code
11010563;New;[v3,01/24] LSM: Infrastructure management of the superblock
11010655;New;[v3,02/24] LSM: Infrastructure management of the sock security
11010565;New;[v3,03/24] LSM: Infrastructure management of the key blob
11010569;New;[v3,04/24] LSM: Create and manage the lsmblob data structure.
11010579;New;[v3,05/24] Use lsmblob in security_audit_rule_match
11010583;New;[v3,06/24] LSM: Use lsmblob in security_kernel_act_as
11010573;New;[v3,07/24] net: Prepare UDS for secuirty module stacking
11010581;New;[v3,08/24] LSM: Use lsmblob in security_secctx_to_secid
11010591;New;[v3,09/24] LSM: Use lsmblob in security_secid_to_secctx
11010593;New;[v3,10/24] Use lsmblob in security_ipc_getsecid
11010601;New;[v3,11/24] LSM: Use lsmblob in security_task_getsecid
11010599;New;[v3,12/24] LSM: Use lsmblob in security_inode_getsecid
11010597;New;[v3,13/24] LSM: Use lsmblob in security_cred_getsecid
11010609;New;[v3,14/24] IMA: Change internal interfaces to use lsmblobs
11010627;New;[v3,15/24] LSM: Specify which LSM to display
11010617;New;[v3,16/24] LSM: Ensure the correct LSM context releaser
11010613;New;[v3,17/24] LSM: Use lsmcontext in security_secid_to_secctx
11010631;New;[v3,18/24] LSM: Use lsmcontext in security_dentry_init_security
11010623;New;[v3,19/24] LSM: Use lsmcontext in security_inode_getsecctx
11010639;New;[v3,20/24] LSM: security_secid_to_secctx in netlink netfilter
11010635;New;[v3,21/24] Audit: Store LSM audit information in an lsmblob
11010643;New;[v3,22/24] LSM: Return the lsmblob slot on initialization
11010649;New;[v3,23/24] NET: Store LSM netlabel data in a lsmblob
11010647;New;[v3,24/24] AppArmor: Remove the exclusive flag
11013907;New;[V3,1/2] selinux: Remove legacy local boolean and user code
11018217;New;[v4,01/23] LSM: Infrastructure management of the superblock
11018299;New;[v4,02/23] LSM: Infrastructure management of the sock security
11018211;New;[v4,03/23] LSM: Infrastructure management of the key blob
11018223;New;[v4,04/23] LSM: Create and manage the lsmblob data structure.
11018227;New;[v4,05/23] LSM: Use lsmblob in security_audit_rule_match
11018219;New;[v4,06/23] LSM: Use lsmblob in security_kernel_act_as
11018231;New;[v4,07/23] net: Prepare UDS for secuirty module stacking
11018235;New;[v4,08/23] LSM: Use lsmblob in security_secctx_to_secid
11018241;New;[v4,09/23] LSM: Use lsmblob in security_secid_to_secctx
11018243;New;[v4,10/23] LSM: Use lsmblob in security_ipc_getsecid
11018251;New;[v4,11/23] LSM: Use lsmblob in security_task_getsecid
11018247;New;[v4,12/23] LSM: Use lsmblob in security_inode_getsecid
11018255;New;[v4,13/23] LSM: Use lsmblob in security_cred_getsecid
11018261;New;[v4,14/23] IMA: Change internal interfaces to use lsmblobs
11018263;New;[v4,15/23] LSM: Specify which LSM to display
11018267;New;[v4,16/23] LSM: Use lsmcontext in security_secid_to_secctx
11018277;New;[v4,17/23] LSM: Use lsmcontext in security_secid_to_secctx
11018271;New;[v4,18/23] LSM: Use lsmcontext in security_dentry_init_security
11018273;New;[v4,19/23] LSM: Use lsmcontext in security_inode_getsecctx
11018287;New;[v4,20/23] LSM: security_secid_to_secctx in netlink netfilter
11018293;New;[v4,21/23] Audit: Store LSM audit information in an lsmblob
11018291;New;[v4,22/23] NET: Store LSM netlabel data in a lsmblob
11018295;New;[v4,23/23] AppArmor: Remove the exclusive flag
11020285;New;[RFC,v2,1/3] x86/sgx: Add SGX specific LSM hooks
11020289;New;[RFC,v2,2/3] x86/sgx: Call LSM hooks from SGX subsystem/module
11020295;New;[RFC,v2,3/3] x86/sgx: Implement SGX specific hooks in SELinux
11027769;New;[1/2] Revert "mcstransd select correct colour range."
11027771;New;[2/2] Fix mcstrans secolor examples
11028415;New;[GIT,PULL] SELinux patches for v5.3
11030367;New;[v5,01/23] LSM: Infrastructure management of the superblock
11030465;New;[v5,02/23] LSM: Infrastructure management of the sock security
11030381;New;[v5,03/23] LSM: Infrastructure management of the key blob
11030371;New;[v5,04/23] LSM: Create and manage the lsmblob data structure.
11030383;New;[v5,05/23] LSM: Use lsmblob in security_audit_rule_match
11030385;New;[v5,06/23] LSM: Use lsmblob in security_kernel_act_as
11030391;New;[v5,07/23] net: Prepare UDS for secuirty module stacking
11030395;New;[v5,08/23] LSM: Use lsmblob in security_secctx_to_secid
11030405;New;[v5,09/23] LSM: Use lsmblob in security_secid_to_secctx
11030401;New;[v5,10/23] LSM: Use lsmblob in security_ipc_getsecid
11030415;New;[v5,11/23] LSM: Use lsmblob in security_task_getsecid
11030411;New;[v5,12/23] LSM: Use lsmblob in security_inode_getsecid
11030409;New;[v5,13/23] LSM: Use lsmblob in security_cred_getsecid
11030423;New;[v5,14/23] IMA: Change internal interfaces to use lsmblobs
11030427;New;[v5,15/23] LSM: Specify which LSM to display
11030435;New;[v5,16/23] LSM: Ensure the correct LSM context releaser
11030445;New;[v5,17/23] LSM: Use lsmcontext in security_secid_to_secctx
11030429;New;[v5,18/23] LSM: Use lsmcontext in security_dentry_init_security
11030441;New;[v5,19/23] LSM: Use lsmcontext in security_inode_getsecctx
11030433;New;[v5,20/23] LSM: security_secid_to_secctx in netlink netfilter
11030449;New;[v5,21/23] NET: Store LSM netlabel data in a lsmblob
11030453;New;[v5,22/23] AppArmor: Remove the exclusive flag
11030457;New;[v5,23/23] SELinux: Verify LSM display sanity in binder
11031697;New;[V3,1/2] libselinux: Save digest of all partial matches for directory
11033739;New;[V3,2/2] setfiles: Update utilities for the new digest scheme
11032537;New;[V3,2/2] setfiles: Update utilities for the new digest scheme
11034081;New;[1/2] libselinux: Save digest of all partial matches for directory
11033941;New;[V4,2/2] setfiles: Update utilities for the new digest scheme
11034435;New;[RFC,v3,1/4] x86/sgx: Add SGX specific LSM hooks
11034437;New;[RFC,v3,2/4] x86/64: Call LSM hooks from SGX subsystem/module
11034453;New;[RFC,v3,3/4] X86/sgx: Introduce EMA as a new LSM module
11034457;New;[RFC,v3,4/4] x86/sgx: Implement SGX specific hooks in SELinux
11035119;New;[V2] libselinux: Fix security_get_boolean_names build error
11038551;New;[RFC] fanotify, inotify, dnotify, security: add security hook for fs notifications
11038601;New;selinux-testsuite: add tests for fsnotify
11042637;New;[RFC] security,capability: pass object information to security_capable
11047687;New;libselinux: Use Python distutils to install SELinux python bindings
11049101;New;[RFC,v2] fanotify, inotify, dnotify, security: add security hook for fs notifications
